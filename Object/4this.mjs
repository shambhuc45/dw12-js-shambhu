let info = {
  firstName: "Shambhu",
  surName: "Chaudhary",
  age: 23,
  isMarried: false,
  fullName: function () {
    console.log(`Hi! This is me ${this.firstName} ${this.surName}`);
  },
};
console.log(info.firstName);
info.fullName();

// 'this' always point itself.
// arrow function doesn't support 'this'.
