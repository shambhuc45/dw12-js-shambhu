// Object is a datatype that stores information in a key value pair.
let info = {
  name: "Shambhu Chaudhary",
  email: "shambhuc45@gmail.com",
  age: 23,
  weight: 68,
  isLoggedIn: true,
};

// Converting Objects into array //Keys

let keysArray = Object.keys(info);
console.log(keysArray);

// Converting Objects into array //values

let valuesArray = Object.values(info);
console.log(valuesArray);

// Converting Objects into array //entries or properties

let propertiesArray = Object.entries(info);
console.log(propertiesArray);
