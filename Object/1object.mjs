// Object is a datatype that stores information in a key value pair.
let info = {
  name: "Shambhu Chaudhary",
  email: "shambhuc45@gmail.com",
  age: 23,
  weight: 68,
  isLoggedIn: true,
};

// Accessing Objects key
console.log(info.name);
console.log(info.email);
console.log(info.age);
console.log(info.weight);
console.log(info.isLoggedIn);

info.email = "shambhuchaudhary030@gmail.com";
console.log(info);
delete info.weight;
console.log(info);
