//Objects into Arrays
//it can be done only to the entries | properties array

let propertiesArray = [
  ["name", "Shambhu Chaudhary"],
  ["email", "shambhuc45@gmail.com"],
  ["age", 23],
  ["weight", 68],
  ["isLoggedIn", true],
];

let objInfo = Object.fromEntries(propertiesArray);
console.log(objInfo); // Object
