// By default if there is no return the function returns an undefined value.

//e.g.
let funct1 = function (num1, num2) {
  let sum = num1 + num2;
};

let s = funct1(1, 2);
console.log(s);
