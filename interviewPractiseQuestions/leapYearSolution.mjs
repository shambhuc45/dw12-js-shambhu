/* 
A year is a leap year if the following conditions are satisfied:

The year is a multiple of 400.
The year is a multiple of 4 and not a multiple of 100.
*/
let leapYearFunction = (year) => {
  if (year % 4 === 0) {
    if (year % 100 === 0) {
      if (year % 400 === 0) {
        return "Leap Year";
      } else {
        return "Not Leap Year";
      }
    } else {
      return "Leap Year";
    }
  } else {
    return "Not Leap Year";
  }
};

let result = leapYearFunction(2008);
console.log(result);
