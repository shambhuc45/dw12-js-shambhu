function bottlesOfBeer() {
  for (let i = 99; i > 0; i--) {
    let bottles = i === 1 ? "bottle" : "bottles";
    let nextBottles = i - 1 === 1 ? "bottle" : "bottles";

    console.log(
      `${i} ${bottles} of beer on the wall, ${i} ${bottles} of beer.`
    );
    console.log(
      `Take one down and pass it around, ${
        i - 1 === 0 ? "no more" : i - 1
      } ${nextBottles} of beer on the wall.`
    );
    console.log("");
  }

  console.log("No more bottles of beer on the wall, no more bottles of beer.");
  console.log(
    "Go to the store and buy some more, 99 bottles of beer on the wall."
  );
}

bottlesOfBeer();
