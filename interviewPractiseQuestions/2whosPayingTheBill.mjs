let persons = [
  "Shambhu",
  "KamalSheel",
  "Sagun",
  "Dikshya",
  "Nitan",
  "Jenis",
  "Sachee",
  "Roshan",
];
let whosPaying = () => {
  let randomPersonIndex = Math.floor(Math.random() * persons.length);
  let randomPerson = persons[randomPersonIndex];

  return `${randomPerson} is paying the bill today`;
};

let result = whosPaying();
console.log(result);
