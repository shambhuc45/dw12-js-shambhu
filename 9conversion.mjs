// Number to String

console.log(String(123)); //"123"

// String to Number
console.log(Number("123")); //123

/* 
all empty  are falsy value

String 
""=> Falsy
all are true even the space(" ");


Number

0 false and
all true
 */

console.log(Boolean("Shambhu"));
console.log(Boolean("")); //false
console.log(Boolean("0"));
console.log(Boolean(" "));
console.log(Boolean(0)); //false
console.log(Boolean(1));
