// array are 0 indexed
// Array Methods Most used

let myArr = [1, 2, 3, 4, 5];

myArr.push(4); // for adding elements inside the array at last
console.log(myArr);

myArr.pop(); // removing last element
console.log(myArr);

myArr.unshift(9); // adds element at first
console.log(myArr);

myArr.shift(); //removes first element
console.log(myArr);

console.log(myArr.includes(9)); // checks if there is equivalent value in the array and returns boolean
console.log(myArr.indexOf(2)); // returns index of an element

//let newArray = myArr.join();
//console.log(newArray); //returns the same array with change in type it will same values but without the curly braces i.e. string

//Slice And Splice

// Slice ( Kateko Tukra)

//if we want sliced / kateko array from the existing one
let katekoArray = myArr.slice(0, 3); // first parameter starting index and second parameter "end index" which won't be included
console.log(katekoArray);

// Splice  It helps to add or remove item from the array

// Removing item    first parameter is index of start and 2nd parameter is no. of item you want to remove after the starting index
let splicedArray = myArr.splice(1, 2); //this loc will splice the array and store the spliced array in the variable splicedArray
console.log(myArr); //original array after spliced
console.log(splicedArray); // the array that is removed / spliced

// Adding Item using Splice
console.log(myArr); //[1,4,5]
myArr.splice(1, 0, 2, 3);
console.log(myArr); // [1,2,3,4,5]
//1st parameter = Index at which to start adding elements (1 in this case)
//second parameter = no. of item to be deleted (0 since we are not removing any)
// others = items to be added in the array (2 and 3 in this case)
