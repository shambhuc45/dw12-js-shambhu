let count = 99;
let bottleSolution = () => {
  if (count > 0) {
    console.log(
      `${count} bottles of beer on the wall, ${count} bottles of beer.`
    );
    count--;

    console.log(
      `Take one down and pass it around , ${count} bottles of beer on the wall`
    );
  } else {
    console.log(
      `No more bottles of beer on the wall, no more bottles of beer.`
    );
    console.log(`Go to Store and buy some more , 99 bottles of beer`);
  }
};
while (count > 0) {
  bottleSolution();
}
