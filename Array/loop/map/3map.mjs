// [1,2,3] = output ([10,11,12])
let ar1 = [1, 2, 3];

let ar2 = ar1.map((val, i) => {
  return val + 10;
});

console.log(ar2);

// ["my", "name", "is"] = output ([ 'MY', 'NAME', 'IS' ])

let myArr = ["my", "name", "is"];
let myArr2 = myArr.map((val, i) => {
  return val.toUpperCase();
});
console.log(myArr2);

//["my", "arr", "is"]= output ([ 'MYN', 'ARRN', 'ISN' ])

let Array1 = ["my", "arr", "is"];

let Array2 = Array1.map((val, i) => {
  return `${val.toUpperCase()}N`;
});

console.log(Array2);

//["my", "arr", "is"] = output ([ 'MY0', 'ARR1', 'IS2' ])

let Arr = ["my", "arr", "is"];

let Arr1 = Array1.map((val, i) => {
  return `${val.toUpperCase()}${i}`;
});

console.log(Arr1);
