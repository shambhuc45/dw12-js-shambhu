// [1,3,4,5] => [100,300,4,500]

let arr = [1, 3, 4, 5];

let arr2 = arr.map((val) => {
  if (val % 2 === 0) {
    return val;
  } else {
    return val * 100;
  }
});

console.log(arr2);
