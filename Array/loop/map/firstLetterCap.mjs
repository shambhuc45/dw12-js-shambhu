let firstLetterCap = (input) => {
  let myArr1 = input.split("").map((val, i) => {
    if (i === 0) {
      return val.toUpperCase();
    } else {
      return val.toLowerCase();
    }
  });
  let result = myArr1.join("");
  return result;
};
let _firstLetterCap = firstLetterCap("shambhu");
console.log(_firstLetterCap);
