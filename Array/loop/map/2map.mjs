let ar1 = [1, 2, 3, 4];

/* 
use map if 
    -input as well as output, both are array.
    -input and output are of same length

*/

let ar2 = ar1.map((value) => {
  return value * 2;
});

console.log(ar2);
