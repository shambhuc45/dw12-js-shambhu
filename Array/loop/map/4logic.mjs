let diceNumberGenerator = (min, max) => {
  let random = Math.random();

  let finalResult = Math.floor(random * (max - min + 1) + min);
  return finalResult;
};

let result = diceNumberGenerator(1, 6);

console.log(result);
