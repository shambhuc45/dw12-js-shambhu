let heroes = ["hulk", "spiderMan", "ironMan", "thor", "captain america"];

heroes.forEach((value, i) => {
  if (value.includes(" ")) {
    value = value.trim();
    let firstSplitWithWhiteSpace = value.split(" ");
    let myArr = firstSplitWithWhiteSpace.map((val, i) => {
      let splittedText = val.split("");
      let mapping = splittedText.map((val, i) => {
        if (i === 0) {
          return val.toUpperCase();
        } else {
          return val.toLowerCase();
        }
      });
      let resultCapitalized = mapping.join("");
      return resultCapitalized;
    });
    let finalResultCapitalized = myArr.join(" ");
    console.log(finalResultCapitalized);
  } else {
    value = value.trim();
    let firstSplitWithWhiteSpace = value.split(" ");
    firstSplitWithWhiteSpace.forEach((val, i) => {
      let splittedText = val.split("");
      let mapping = splittedText.map((val, i) => {
        if (i === 0) {
          return val.toUpperCase();
        } else {
          return val.toLowerCase();
        }
      });
      let resultCapitalized = mapping.join("");
      console.log(resultCapitalized);
    });
  }
});

//"my name is nitan" =>"My Name Is Nitan"
