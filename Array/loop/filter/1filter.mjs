/* 
Filter returns Boolean value only
by default the filter returns false



just like map,In filter method input and output both should be an array.
    and it doesn't matter the length of the output(meaning output length can vary according to the logic)
    [1,2,3,4] = [1,2] ok
    [1,2,3,4] = [1,2,3]  ok
    [1,2,3,4] = [1,5] not okay
*/

//Filtering even numbers
let ar1 = [1, 2, 3, 4];
let ar2 = ar1.filter((value, index) => {
  if (value % 2 === 0) {
    return true;
  } else {
    return false;
  }
});

console.log(ar2);

//filtering Odd numbers
let arr1 = [1, 2, 3, 4];
let arr2 = ar1.filter((value, index) => {
  if (value % 2 === 0) {
    return false;
  } else {
    return true;
  }
});

console.log(arr2);
