//find Method
// find is run to find only one true match
//returns first match only

let arr = [1, 2, 3, 4];
let result = arr.find((value, i) => {
  if (value > 3) {
    return true;
  } else {
    return false;
  }
});
console.log(result); //4
