// checks every item in an array according to the
// condition provided and gives output true if all of the element returns true
let ar1 = [13, 25, 17];
let isGreaterThan18 = ar1.every((val, i) => {
  if (val > 18) {
    return true;
  }
});
console.log(isGreaterThan18);

// check whether we have all even numbers in the array [2,4,6,9]

let myArr = [2, 4, 6, 9];
let evenCheck = myArr.every((val, i) => {
  if (val % 2 === 0) {
    return true;
  }
});
console.log(evenCheck);

// check all students are passed passmark is 40 [40,30,80]
let marks = [40, 30, 80];
let allPassed = marks.every((val, i) => {
  if (val >= 40) {
    return true;
  }
});

if (allPassed === true) {
  console.log("All Students are passed.");
} else {
  console.log(`Some students aren't passed.`);
}
console.log(allPassed);
