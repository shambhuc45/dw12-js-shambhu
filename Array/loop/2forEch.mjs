let heroes = ["hulk", "spiderMan", "ironMan", "thor", "captain america"];

heroes.forEach((value) => {
  let firstSplitWithWhiteSpace = value.split(" ");
  firstSplitWithWhiteSpace.forEach((val, i) => {
    let splittedText = val.split("");
    let mapping = splittedText.map((val, i) => {
      return i === 0 ? val.toUpperCase() : val.toLowerCase();
    });
    let correctedArray = mapping.join("");
    console.log(correctedArray);
  });
});
