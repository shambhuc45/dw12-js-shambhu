// Number sort ascending sort
let myArr = [0, 5, 2, 7, 9];

let ascNumberSort = myArr.sort((a, b) => {
  return a - b;
});
console.log(ascNumberSort);

// Number sort decending
let myArr1 = [0, 5, 2, 7, 9];

let descNumberSort = myArr.sort((a, b) => {
  return b - a;
});
console.log(descNumberSort);

// Sorting string according to string length
let myArr2 = ["ab", "a", "abcd"];

let ascStringSort = myArr2.sort((a, b) => {
  return a.length - b.length;
});
console.log(ascStringSort);
