// Sort method
let ar1 = ["b", "c", "a"];
let ar2 = ar1.sort();
console.log(ar1);

let ar3 = ar2.reverse();
console.log(ar3);

/* Js doesn't support number sorting because js compares digit wise if the first digit of a bigger number is
smaller than the smaller number there will be conflict.
 */

// example
let arr = [10, 9];
let arr2 = arr.sort();
console.log(arr2); // [ 10, 9 ]

// Js compares characters according to their ASCII value like Capital letter have lesser Ascii value
// than small letters so during sort the Capital letter comes before small

let myar1 = ["a", "b", "Z"];
let myar2 = myar1.sort();
console.log(myar2); // [ 'Z', 'a', 'b' ]


