// Array is a sequence of related variables although it can store different types of data.
let names = ["shambhu", "ram", "sagun", "kamalsheel"];
// console.log(names); //retrieving whole array

//retrieving individual names
console.log(names[0]);
console.log(names[1]);
console.log(names[2]);

names[1] = "shyam";
console.log(names);
