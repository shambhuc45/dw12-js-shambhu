// let info = {
//   name: "shambhu",
//   age: 23,
//   isMarried: false,
// };

//variable name must be same
// in object order doesn't matter.
let { name, age, isMarried } = {
  name: "shambhu",
  age: 23,
  isMarried: false,
};
console.log(name);
console.log(age);
console.log(isMarried);
