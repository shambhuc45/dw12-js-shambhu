let name = "Shambhu Chaudhary Shambhu";

console.log(name.length);
// console.log(name.toUpperCase());
// console.log(name.toLowerCase());
// console.log(name.trim());
// console.log(name.trimStart()); //removes Whitespaces at the start of the value.
// console.log(name.trimEnd()); //removes Whitespaces at the end of the value.
// console.log(name.startsWith("Sham")); //checks if the provided value is same with the starting of the stored value.
// console.log(name.endsWith("Chaudhary")); //checks if the provided value is same with the ending of the stored value.
console.log(name.replace("Shambhu", "lalit")); //replaces the first match only.First Shambhu will be replaced with the "Lalit"
console.log(name.replaceAll("Shambhu", "Lalit")); // replaces all matching keywords.

let myTrim = (args) => {
  return args.trim();
};
let resultMyTrim = myTrim("   nitan   ");
console.log(resultMyTrim);

console.log(name.split(" ")); // split method returns an array which is separated by the whiteSpace
