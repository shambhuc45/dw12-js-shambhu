// undefined : is a variable datatype that is defined but not touched or not assigned any value to it.
// null :  null means a variable is defined and it is initialized to empty.

let a = null;
console.log(a);

console.log(typeof a); //object

// typeof(null) is an object which is bug in js
