// num is less than or equal 39   fail
// num [40-59]  third division
//  num [ 60-79]  First division
//  num [80-100]  distinction

let num = 60;
let grade =
  num < 40
    ? "Fail"
    : num >= 40 && num < 60
    ? "Third Division"
    : num >= 60 && num < 80
    ? "First Division"
    : num >= 80 && num <= 100
    ? "Distinction"
    : null;
console.log(grade);

// there must be the else part in ternary operator
