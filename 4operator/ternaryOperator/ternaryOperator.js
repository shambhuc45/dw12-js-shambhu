// above 18 "Adult" else "Underage"

let age = 17;
// let category = age >= 18 ? "Adult" : "Underage";
// console.log(category);

// less than 18 "Underage"
// 18 or above and less than 50 "Adult"
// else "Old"

let ageGroup =
  age > 0 && age < 18 ? "Underage" : age >= 18 && age < 50 ? "Adult" : "Old";
console.log(ageGroup);

// num is less than or equal 39   fail
// num [40-59]  third division
//  num [ 60-79]  First division
//  num [80-100]  distinction

let num = 60;
let grade =
  num < 40
    ? "Fail"
    : num >= 40 && num < 60
    ? "Third Division"
    : num >= 60 && num < 80
    ? "First Division"
    : "Distinction";
console.log(grade);
