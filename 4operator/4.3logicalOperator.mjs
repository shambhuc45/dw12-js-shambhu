// && , ||, !

//&& = true if all are true

console.log(true && true && false); // false

// || = true if one is true

console.log(false || true || false); //true

console.log(!false); //true
