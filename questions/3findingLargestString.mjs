// from array find that element whose length is highest in javascript

let myArray = ["shambhu", "kamalsheel", "sagun", "nitan"];

let largestString = myArray.reduce((prev, curr) => {
  if (prev.length > curr.length) {
    return prev;
  } else {
    return curr;
  }
}, " ");
console.log(largestString);

let largestString1 = (myArray) => {
  let largestStringboom = " ";
  for (let i = 0; i < myArray.length; i++) {
    if (largestStringboom.length < myArray[i].length) {
      largestStringboom = myArray[i];
    }
  }
  return largestStringboom;
};
console.log(largestString1(myArray));

// finding shortest string

let myArr = ["shambhu", "kamalsheel", "sagun", "nitan"];

let smallestString = myArr.reduce((prev, curr) => {
  if (prev.length < curr.length) {
    return prev;
  } else {
    return curr;
  }
});
console.log(smallestString);
