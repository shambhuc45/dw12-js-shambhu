// Q.N.1
let is18 = function (age) {
  if (age === 18) {
    return true;
  } else {
    return false;
  }
};

let resultIs18 = is18(17);
console.log(resultIs18);

// Q.N.2
let entryHandler = function (number) {
  if (number < 18) {
    return "You can enter the room";
  } else {
    return "You cannot enter. Please proceed to Adults room";
  }
};
let resultEntryHandler = entryHandler(19);
console.log(resultEntryHandler);

//Q.N.3
let isEven = function (number) {
  if (number % 2 === 0) {
    return true;
  } else {
    return false;
  }
};
let resultIsEven = isEven(18);
console.log(resultIsEven);

//Q.N.4

let categoryHandler = function (number) {
  if (number >= 0 && number <= 10) {
    return "Category1";
  } else if (number >= 11 && number <= 20) {
    return "Category2";
  } else if (number >= 21 && number <= 30) {
    return "Category3";
  } else {
    return "Please Enter a number from range 1 to 30 only";
  }
};
let resultCategoryHandler = categoryHandler(12);
console.log(resultCategoryHandler);

//Q.N.5
let checking3 = function (number) {
  if (number === 3) {
    return "I am 3";
  } else if (number >= 3) {
    return "I am greater or equal to 3";
  } else if (number < 3) {
    return "I am less than 3";
  } else {
    //this will be executed only if the user provides null value or value other than number like string,array(having more than 1 item),object
    return "I am Other";
  }
};
let resultChecking3 = checking3(2);
console.log(resultChecking3);

//Q.N.6

let movieAuthorityHandler = function (number) {
  if (number >= 18) {
    return "You can watch this movie";
  } else {
    return "You are not eligible to watch this movie. Come after Your 18th birthday";
  }
};
let resultMovieHandler = movieAuthorityHandler(17);
console.log(resultMovieHandler);
