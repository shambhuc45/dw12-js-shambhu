let products = [
  {
    id: 1,
    title: "Tech-Television",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "a1",
    },
  },
  {
    id: 2,
    title: "Hoodie",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "a2",
    },
  },
  {
    id: 3,
    title: "FanTech",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "a3",
    },
  },
];

// find the array of id ie  output must be [1,2,3]

let ids = products.map((value, i) => {
  return value.id;
});
// console.log(ids);

//find the array of title ie output must be ["Product 1", "Product 2", "Product 3"]

let titles = products.map((value, i) => {
  return value.title;
});
// console.log(titles);

//find the array of category
let categories = products.map((value, i) => {
  return value.category;
});
// console.log(categories);

// find the array of type
let types = products.map((value, i) => {
  let discount = value.discount;
  return discount.type;
});
// console.log(types);

// find the array of price where each price is multiplied by 3  output must be [ 15000,6000,9000]

let pricesxThree = products.map((value, i) => {
  return value.price * 3;
});
// console.log(pricesxThree);

// find those array whose price is >= 3000

let arrayHavingPriceAbove3000 = products.filter((value, i) => {
  if (value.price >= 3000) {
    return true;
  }
});
// console.log(priceAbove3000);

//find those array of  title whose price is >= 3000=>["product 1",product 3]
// If filter and map are used simultaneously,always use filter first

let titlesHavingPriceAbove3000 = products
  .filter((value, i) => {
    // if (value.price >= 3000) return true;
    return value.price >= 3000 ? true : false;
  })
  .map((value, i) => {
    return value.title;
  });
console.log(titlesHavingPriceAbove3000);

//find those array of title whose price does not equal to 5000 ==> ["product 2","product 3"]
let titlesHavingPriceOtherThan5000 = products
  .filter((value, i) => {
    if (value.price !== 5000) return true;
  })
  .map((val, i) => {
    return val.title;
  });
// console.log(titlesHavingPriceOtherThan5000);

//find the product Names which contains Tech in their name

let arrayofProductsHavingTechInTitle = products
  .filter((value, i) => {
    if (value.title.includes("Tech")) {
      return true;
    }
  })
  .map((val, i) => {
    return val.title;
  });

//find those array of category whose price equal to 3000 ===> ["electronics"]
let categoryoftheProductAbove3000 = products
  .filter((value, i) => {
    if (value.price === 3000) return true;
  })
  .map((val, i) => {
    return val.category;
  });
// console.log(categoryoftheProductAbove3000);
