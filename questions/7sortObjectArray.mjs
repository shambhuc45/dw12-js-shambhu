let products = [
  {
    name: "earphone",
    price: 200,
  },
  {
    name: "battery",
    price: 5000,
  },
  {
    name: "charger",
    price: 800,
  },
];
console.log(products);

// sorting according to its price
let priceSort = products.sort((a, b) => {
  return a.price - b.price;
});
console.log(priceSort);

//
