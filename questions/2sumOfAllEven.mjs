let myarr1 = [1, 2, 3, 4, 5, 6];

let myarr2 = myarr1.filter((value, i) => {
  if (value % 2 === 0) {
    return true;
  } else {
    return false;
  }
});

let sum = myarr2.reduce((prev, curr) => {
  return prev + curr;
}, 0);
console.log(sum);
