// The block gets executed only if the condition is true
if (true) {
  console.log("Hello i am If");
}

let name = "Shambhu";
if (name === "Shambhu") {
  console.log("Hello Shambhu");
}

let a = "hari";
if (a) {
  console.log("Hello");
}
