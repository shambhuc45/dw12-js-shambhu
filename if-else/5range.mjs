let age = 150;

if (age >= 0 && age <= 18) {
  console.log("Underage");
} else if (age > 18 && age <= 60) {
  console.log("Adult");
} else if (age > 60 && age <= 150) {
  console.log("Old");
} else {
  console.log("None");
}
