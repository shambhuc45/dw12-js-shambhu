let ar1 = ["a", "b"];
let ar2 = ["x", "y", "z"];
let ar3 = [1, 2, ...ar2, 4, ...ar1]; //[1,2,"x", "y", "z",4,"a", "b"]
console.log(ar3);

//Spread Operator(...) is nothing but a wrapper opener.
//Its is used to make new array from the existing array.
