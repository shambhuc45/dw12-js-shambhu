let name = "Shambhu" || "Chaudhary" || "Koteshwor";
console.log(name);

let myName = false || "" || undefined || 0 || null;
console.log(myName);

//if the value is falsy while moving from the left to right then move to the next value
// if all are false the last value will be the output