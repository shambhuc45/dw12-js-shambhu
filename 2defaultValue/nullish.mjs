let myName = false ?? "" ?? undefined ?? 0 ?? null;
console.log(myName); //false

let name = null ?? undefined ?? undefined ?? 0 ?? null;
console.log(name); //0

let name2 = null ?? undefined ?? undefined ?? null ?? null;
console.log(name2); //null

//In nullish case, Only "null" and "undefined" are falsy value.
//if the value is falsy while moving from the left to right then move to the next value
// if all are false the last value will be the output
