//  (==) compares the value doesn't care about the type.
// (===) compares value as well as type.

console.log(1 == 1); //true
console.log(1 === 1); //true

console.log("1" == 1); //true
console.log("1" === 1); //false
