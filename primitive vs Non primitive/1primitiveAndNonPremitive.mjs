// Primitive
// in case of primitive , new space is created if let keyword is used
// e.g.
//     let a =1 ;
//     let b=a;
//     let c=1
//     a=10
//     console.log(a)//10
//     console.log(b)//1
//     console.log(c)//1
//       ===(value)
//               -in primitive the === produce true if the value are same

// Non-Primitive:
// in case of non-primitive, a new memory space is created if a variable isn't copy of another variable
// if a variable is a copy of another variable the variable share the memory

// e.g.
    let a = [1,2]
    let b=a;
     a =  [1,2,3,4]
    let c=[1,2]
    a.push(3);
    console.log(a)
    console.log(b)
    console.log(c)
    console.log(a===b)

//         === (memory location)
//            non primitive the === produce true if they share the same memory location
