/*  Asynchronous Function:
        Anything that push its task to background (node) are called asynchronous function.
        During Code execution, The background code will be executed when all synchronous js code gets executed.
        Callstack run the code inside it. once the code gets executed, the code is popped off(removed from callStack).
        Event Loop is a mediator which continously monitor callStack and Memory queue.if the call stack is empty, it push the function from memory queue to callStack.
*/
console.log("1");
setTimeout(() => {
  console.log("3");
}, 0);
console.log("1");
console.log("1");
console.log("1");
console.log("1");
console.log("1");
console.log("1");
console.log("1");
console.log("1");
console.log("1");
