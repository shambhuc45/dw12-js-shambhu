let currentDateTime = new Date();
console.log(currentDateTime);

//valid ISO format  (yyyy-mm-ddThh:mm:ss)
// 2024/02/21T04:57:10

//invalid ISO format
// 2024/2/21T4:57:10   must be two digits in month and the hour i.e. 02 for month(2) and 04 for hour(4)
